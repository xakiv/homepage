# Local dev

Get latest release `.deb` package from: `https://github.com/gohugoio/hugo/releases/`  

Grab homepage sources:  
```commandline
git clone git@gitlab.cbh:xakiv/homepage.git
```
Grab theme sources:  
```commandline
git submodule update --init
```
Run dev web server: `http://localhost:1313/homepage/`  
```commandline
hugo server -D 
```
Create new content
```
hugo new content pages/post_X.md
```
In case theme sources were updated on remote (https://github.com/nanxiaobei/hugo-paper):  
```commandline
git submodule update --remote --merge
```