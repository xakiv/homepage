---
title: "Djambo"
date: 2024-10-04T10:24:56+02:00
draft: false
description: "..."
---

### Repo: [Djambo](https://gitlab.com/xakiv/djambo)

I needed a cosy place where I could deploy some django applications 
to show off how ugly my Frontend skills are... 😭  
A colleague of mine talked me about [alwaysdata](https://www.alwaysdata.com/en/) and their free plan.  
They got a quite good documentation helping to deploy a python project:  
[Python Configuration](https://help.alwaysdata.com/en/languages/python/configuration/)  
And I used this french blog post as a helpful tutorial:  
[FastAPI Asyncio Configuration](https://pereprogramming.com/articles/comment-deployer-fastapi-chez-alwaysdata/)  


The website is available @ [xakiv.alwaysdata.net](https://xakiv.alwaysdata.net)[Archived]  
More recently I get a dedicated server at OVH Cloud and deployed djambo containerized with docker:  
The website is available @ [xakiv-djambo.hopto.org](http://xakiv-djambo.hopto.org/)  


At this moment, two apps are installed:  
- [xml converter](https://gitlab.com/xakiv/djambo/-/tree/main/backend/xakiv_converter)  
- [avatar generator](https://github.com/xakiv/multiavatar-python/)  (packaged with poetry)  
The latest is a factorized fork from the fun project of Gie Katon ([multiavatar](https://github.com/multiavatar/multiavatar-python)).  

