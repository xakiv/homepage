---
title: "Multivatar Fork"
date: 2023-10-25T00:54:25+02:00
draft: false
description: "..."
---
### Repo: [Multivatar Fork](https://github.com/xakiv/multiavatar-python)

I forked the fun project of Gie Katon ([multiavatar](https://github.com/multiavatar/multiavatar-python)).  
A python implementation of the random avatar generator.  
The purpose of [my fork](https://github.com/xakiv/multiavatar-python) was to play around with Jinja templating.  
And seprating style settings, templating and logical process.