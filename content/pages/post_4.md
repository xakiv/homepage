---
title: "Totor"
date: 2023-10-25T01:17:02+02:00
draft: false
description: "..."
---
### Repo: [Totor](https://gitlab.com/xakiv/torrent_csv_cli)

Yet another Torrent-CSV CLI, in python ! Maybe there is enough Torrent-CSV CLI 🫣...  
But I went ahead and created a packaged python "application"  
That way, it can be easly installed with  
```
pip install git+https://gitlab.com/xakiv/torrent_csv_cli.git
```

Totor is basically an [ETL](https://en.wikipedia.org/wiki/Extract,_transform,_load):  
- Extract data from torrent-csv API,  
- Transform those data so they can be exploitable later,  
- "Load" them to the end-user.  

It was a fun project where I tested [requests-cache](https://requests-cache.readthedocs.io/en/stable/).  
As some third party data (trackers list) are only updated once a day,  
I wanted to retreive those data only once a day.  
Doing so avoids to send requests on the web for each CLI calls.  

As the data are paginated, it was a great excuse to crawl through the data  
with an iterable object  

>[totor/cli.py](https://gitlab.com/xakiv/torrent_csv_cli/-/blob/main/totor/cli.py?ref_type=heads#L42)
```
(...)
    for crawler in Totor(args.query, args.size, 1, args.format, args.trackers):
        crawler()
        fetch = input("--> Fetch next page ? [y/N]")
        if fetch.lower() in ["y", "yes", "yup", "true"]:
            continue
        else:
            break
(...)
```
Which was possible by defining `__iter__` and `__next__` methods  

>[totor/totor.py](https://gitlab.com/xakiv/torrent_csv_cli/-/blob/main/totor/totor.py?ref_type=heads#L20)
```
class Totor:
    (...)
    def __init__(self, query, size, page, fmt, trackers_list):
        self.query = query
        self.size = size
        self.page = page
        self.format = fmt
        self.trackers = self.load_trackers(trackers_list)
    (...)
    def __iter__(self):
        return self

    def __next__(self):
        try:
            self.page += 1
            return self
        except Exception:
            raise StopIteration
    (...)
    def __call__(self):
        self.handle_request()
        self.format_results()
```
