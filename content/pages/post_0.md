---
title: "Post 0"
date: 2023-07-12T01:12:59+02:00
draft: false
description: "..."
---

Publishing this site is mostly an excuse to test [Hugo](https://gohugo.io/about/what-is-hugo/)  
a static site generator with the [Paper theme](https://themes.gohugo.io/themes/hugo-paper/).