---
title: 'An Odoo journey'
date: '2023-12-18T14:52:18+01:00'
draft: false
description: "..."
---

### Repo: [Park](https://gitlab.com/xakiv/park)

After reading [this tutorial](https://jortdevreeze.com/blog/odoo-2/dock-stack-launch-odoo-deployment-with-docker-and-portainer-4),  
I wanted to setup a small playground for an Odoo app.  

### Step One: 
Setting up a network through portainer is a bit overkill for what I want to try.  
So let's just start with a simple `docker-compose.yml` configuration:  

`park/docker-compose.yml`
```dockerfile
version: "3.8"

services:

  postgres:
    container_name: odoo-postgres
    image: postgres:14.1-alpine
    ports:
      - "5555:5432"
    environment:
      POSTGRES_DB: postgres
      POSTGRES_USER: odoo
      POSTGRES_PASSWORD: odoo
    volumes:
      - postgres-data:/var/lib/postgresql/data
    restart: on-failure:3

  odoo:
    container_name: odoo
    image: odoo:16.0
    environment:
      HOST: postgres
      USER: odoo
      PASSWORD: odoo
    depends_on:
      - postgres
    ports:
      - "8069:8069"
    volumes:
      - data:/var/lib/odoo
      - ./config:/etc/odoo
      - ./extra-addons:/mnt/extra-addons
    restart: on-failure:3
    links:
      - postgres

volumes:
  postgres-data:
  config:
  extra-addons:
  data:
```
We bind some of the Odoo volumes with `park/config` and `park/extra-addons` directories.  
That way we will be able to create and sync our Odoo configuration files and our extra-addons  
between our host machine and the docker instance.

`park/config/odoo.config`  
```
[options]
addons_path = /mnt/extra-addons
data_dir = /var/lib/odoo
```
Then we can create our empty Odoo app in a Python package named `park/extra-addons/wtf`  
with this `park/extra-addons/wtf/__manifest__.py` configuration file:  

```
{
    'name': 'WTF',
    'version': '1.0',
    'summary': 'We Tweek [F]ings',
    'author': 'xakiv',
    'website': 'https://xakiv.gitlab.io/homepage/pages/post_6/',
    'depends': [
        'base',
    ],
    'data': [
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
```

Building and running this stack with: `docker-compose up --build`  

allows us to access to the Odoo database setup view: `http://127.0.0.1:8069/web/database/selector`  
We make sure to use the correct passwords.  
In `park/docker-compose.yml` we defined them as environment variables for `postgres` and `odoo` services.  

![Database Manager View](img/odoo-database-manager-init.png)

We can now login with the credentials:  
![Login View](img/odoo-login-init.png)

And _tada_, here is our empty Odoo apps that does nothing, along side with its buddies  
![Apps List](img/odoo-apps-list.png)

### Step Two: 
We will implement some Models, Views, Controler ...