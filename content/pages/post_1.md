---
title: "Dot Files"
date: 2024-02-25T00:54:21+02:00
draft: false
description: "My lovely dotfiles..."
---
### Repo: [Dev.env Generator (Archived)](https://gitlab.com/xakiv/dev-env-generator/)

This is a collection of bash scripts that help setting up my local dev machine.  
It's inspired from [SimpleSH](https://github.com/rafaelstz/simplesh) and adapted with the tools I actually use for my day to day work.  
I also cherry picked some idea from [Feroldi ricing](https://github.com/feroldi/ricing) doc.  


### New Repo: [Dot](https://gitlab.com/xakiv/dot/)
Since then, I switched to another repository with updated recipes and versioned  
`etc/` configuration files.  