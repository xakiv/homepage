---
title: "A Small FastAPI GIS application"
date: 2024-08-25T11:28:12+02:00
draft: false
description: "..."
---

### Repo: [Peaks](https://github.com/xakiv/peaks)

This is the result of a technical assessment.  
The main task was to deliver a small FastAPI application, that handles CRUD operations on geospatial data.  
The application is dockerized and tests are available for a coverage score greater than 90%  
  
It was a good place to learn more about Github Actions.  
I created a docker image publishing [workflow](https://github.com/xakiv/peaks/blob/main/.github/workflows/docker_image.yml).  
And another [workflow](https://github.com/xakiv/peaks/blob/main/.github/workflows/run_tests.yml) that runs tests when new features are pushed.  
Unfortunately the later action is not yet ready.

