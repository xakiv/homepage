---
title: "About me"
date: 2025-01-06T11:02:08+01:00
draft: false
weight: 10
description: "..."
---
Not much 😅,  
I'm a backend developer (Python / Django) based in France (Toulouse).  


You can find my up-to-date resume ([EN](cv/en/BENHABIB-Chakib-eng-dev.pdf) / [FR](cv/fr/BENHABIB-Chakib-fr-dev.pdf)).

And my profiles:  
- [https://linkedin.com/in/chakib-benhabib/](https://linkedin.com/in/chakib-benhabib/)
- [https://gitlab.com/xakiv/](https://gitlab.com/xakiv/)
- [https://github.com/xakiv/](https://github.com/xakiv/)