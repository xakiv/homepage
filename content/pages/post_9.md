---
title: "Bookmarks"
date: 2024-09-10T09:37:49+02:00
draft: false
weight: 11
description: "..."
---

Dev online tools:  
- [https://jsonformatter.curiousconcept.com/#](https://jsonformatter.curiousconcept.com/#)
- [https://regex101.com/](https://regex101.com/)
- [https://newreleases.io/](https://newreleases.io/)
- [https://httpie.io/app](https://httpie.io/app)
- [https://phosphoricons.com/](https://phosphoricons.com/)

Reference resources:
- [https://refactoring.guru/design-patterns/python](https://refactoring.guru/design-patterns/python)
- [https://realpython.com/](https://realpython.com/)

  Blog posts:  
- [https://dev.to/](https://dev.to/)
