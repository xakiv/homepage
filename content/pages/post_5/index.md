---
title: "Wordpress Pharma Hack"
date: 2023-11-08T16:27:18+01:00
draft: false
description: "..."
---

Sometimes, the universe asks you to do something that you do not enjoy...  
When a friend of mine had an issue with their Wordpress site I knew it was those kind
of times.  

I don't know how Wordpress works globally. 
but here is how I __tried__ to handle the issue.  

## First, the "What" ?  
When looking for the content related to the site indexed by our almighty search engine,  
we can see some strange sitelinks (mostly for drugs advertising):

![Google Search Results](img/google-sitelinks-blur.png)

That said, when browsing through the links with a standard browser,
the proper content and title are correctly loaded.  

But if we spoof our User-Agent with the one used by google's crawler:  
`Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)`

Using either a browser extension like [User Agent Switcher](https://addons.mozilla.org/en-US/firefox/addon/user-agent-string-switcher/)
or overriding the request header with [Postman](https://blog.postman.com/what-are-http-headers/)  
Then we are going to see some very odd contents with strange html formatting.  
![Strange Content 1](img/odd-pharma-hack.png)  

Inspecting the php source we can also find some odd scripts :  

![Strange Content 2](img/wp-plugins.png)
![Strange Content 3](img/wp-uploads.png)

with this kind of content :  

![Strange Content 4](img/strange-content.png)

Deleting those scripts and cleaning the database did not had the expected result...  
![A few moments later](img/a-few-moments-later.jpg)
Two hours later I found out more suspicious files and a little surprise, __ze Backdoor__:  

![Ze Backdoor](img/admin-backdoor.jpg)

Sometimes, the universe knows how to say "f*** you".

## Then, the "How" ?  
Stay calm, stop running, enjoy the moment, start cleaning:  
- Backup the site, the theme configuration and the database,
- update credentials for admin dashboard, ftp, database,
- erase old source code, old database,
- load an up-to-date Wordpress and install a clean database,
- load the old posts content and theme configuration,
- install and configure [Wordfence plugin](https://wordpress.org/plugins/wordfence/),
- disable the remote access to `xmlrpc.php` by adding the following conf to the `.htaccess`

```commandline
# Block WordPress xmlrpc.php requests
<Files xmlrpc.php>
order deny,allow
 deny from all
</Files>

# END WordPress xmlrpc.php requests
```
